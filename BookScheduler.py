# Author:   Will Bixler
# Created:  09/20/2018
# Bugs:     None
#
# Description:
#   A simple application to calculate how many pages you need to read to finish
#   a book in x number of days. Outputs how many pages per day and what page
#   the reader needs to be on by the end of each day.

import math

# Ask if it's the NET+ book
net = input("Is this for the Net+ book? (y/n) ")

# If it's the NET+ book
if net == "y":
    # NET+ is 631 pages
    pages = 631
else:
    # Ask if it's the A+ book
    a_plus = input("Is this for the A+ book? (y/n) ")
    if a_plus == "y":
        # The A+ book is 1000 pages
        pages = 1000
    else:
        # Ask how many pages the book is
        pages = int(input("How many pages do you have to read? "))

# How many days does the reader have and what page are they on now?
days = int(input("How many days do you have? "))
page = int(input("What page are you on? "))

# Calc pages per day
pages_per_day = (pages - page)/days

# Output pages per day
print("\n{:d} Pages Per Day\n".format(math.ceil(pages_per_day)))

# Figure out each day's goal
for day in range (0,days):

    # User must be on this page by the end of day
    est_page = math.ceil( page + pages_per_day * (day + 1) )

    # Output data
    print("Day: {:d} \tPage: {:d}".format((day+1),est_page))

# Pause to show user
input("\nPress Enter to continue...")
