# Author:   Will Bixler
# Created:  9/20/2018
# Bugs:     None
#
# Descriptions:
#   The code asks the user for the square footage of a room and the length/width
#   ratio. From that, it estimates how wide and how long the room is in feet.

import math

# Get room square footage
try:
    area = float(input("What is the total square footage?"))
except:
    print("Not an integer value.")
    exit()

# Get width ratio
try:
    widthRatio = float(input("What is the width ratio? "))
except:
    print("Not an integer value.")

# Get length ratio
try:
    lengthRatio = float(input("What is the length ratio? "))
except:
    print("Not an integer value.")

# Calculate width and length
width = math.sqrt(area * widthRatio / lengthRatio)
length = area / width

# Output results
print("Our best estimate is {0}ft x {1}ft".format(width,length))
