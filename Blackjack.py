# Author:   Will Bixler
# Created:  9/20/2018
# Bugs:     None
#
# Description:
#   A simple blackjack game: try to get 21. 21 is instant victory and going
#   over is instant loss. After your turn, the AI will try to beat your number
#   without going over 21.

import random
import time

# Player's turn
def player_turn():

    # Init player's hand
    hand = random.randrange(2,20)

    # Show hand
    print("Hand: " + str(hand))

    # Until the player stays
    while True:
        # Ask if player wants to hit or stay
        hit = input("\nHit or stay? (hit/stay) ")
        # If he doesn't hit
        if hit != "hit":
            # End player turn and return hand
            return hand

        # Deal another card
        hand += random.randrange(1,10)

        # Show hand
        print("Hand: " + str(hand))

        # If player busted
        if hand > 21:
            # Player lost
            print("\nBUST!")
            bust = True
            return hand
        # If player got 21
        elif hand == 21:
            # Player won
            print("\n21! You Won!")
            won = True
            return hand

# AI's turn
def ai_turn(hand):

    # Init AI's hand
    ai_hand = random.randrange(2,20)

    # Show hands
    print("You: " + str(hand))
    print("Opponent: " + str(ai_hand))

    # If AI's hand beats player's hand
    if ai_hand > hand:
        # AI won
        print("Opponent Won.")
        return

    # Until AI busts
    while True:
        # Wait 1 second
        time.sleep(1)
        # Hit AI
        print("\nOpponent Hit")
        ai_hand += random.randrange(1,10)
        # Wait 1 second
        time.sleep(1)
        # Show hands
        print("You: " + str(hand))
        print("Opponent: " + str(ai_hand))

        # If AI busted
        if ai_hand > 21:
            # AI lost
            time.sleep(1)
            print("\nOpponent Busted! You Won!")
            return
        # If AI got 21
        elif ai_hand > hand:
            # AI won
            time.sleep(1)
            print("\nOpponent Won.")
            return

# Ask player to play
answer = input("Do you want to play blackjack? (y/n) ")

# If player doesn't say yes
if answer != "y":
    # Exit
    print("Goodbye")
    exit()
else:
    # Play game
    playing = True

while playing:

    hand = player_turn()

    if hand < 21:
        ai_hand = ai_turn(hand)

    # Ask to play again
    answer = input("\nWould you like to play again? (y/n)")

    # If player doesn't say yes
    if answer != "y":
        # Exit
        print("Goodbye")
        exit()
    else:
        # Play game
        playing = True
